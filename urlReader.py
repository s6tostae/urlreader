import urllib.request
import gzip
import json
import os

class UrlReader():

    def __init__(self, url):
        self.url = url


    def openUrl(self):
        self.req = urllib.request.Request(self.url)
        self.req.add_header('Accept-Encoding', 'gzip')

        data = None

        try:
            response = urllib.request.urlopen(self.req)

            if response.headers['Content-Encoding'] == 'gzip':
                data = gzip.GzipFile(fileobj=response).read()
            else:
                data = response.read()

        except urllib.error.URLError as e:
            print(e.reason)
        except urllib.error.HTTPError as e:
            print(e.code)

        return data



    def getDataResponse(self):
        data = self.openUrl()
        return data


    def getJsonResponse(self):
        data = json.loads(self.getDataResponse())
        return data

    def getFileResponse(self, dest_dir):
        data = self.getDataResponse()
        result = urllib.parse.urlparse(self.url)
        outpath = os.path.basename(result.path)

        f = open(dest_dir + outpath, "wb")
        f.write(data)
        f.close()


if __name__ == '__main__':
    url = "https://www.pegelonline.wsv.de/webservices/rest-api/v2/stations.json"
    u = UrlReader(url)
    u.getFileResponse("C:/temp/")